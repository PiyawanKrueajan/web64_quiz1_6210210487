import logo from './logo.svg';
import './App.css';
import AboutUsPage from './page/AboutUsPage';
import Header from './components/Header';
import PredictedPage from './page/PredictedPage';
import { Routes, Route } from "react-router-dom";


function App() {
  return (
    <div className="App">
        <Header />
        
		<Routes>
			<Route path="about" element={
				<AboutUsPage />
			} />
		  
			<Route path="/" element={
				<PredictedPage />
			} />  
			
		</Routes>
        
    </div>
  );
}

export default App;
				
