import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function Predicted (props) {



    return (
		
			<Box sx = {{ width : "90%" ,  }}>
				<Paper elevation= {2} >
					<h3>คุณ: {props.name} </h3>
					<h3>ผลการทำนาย: {props.horoscope} </h3>
					<h3>แปลว่า: {props.result} </h3>
				</Paper>
			</Box>
		
    );
}

export default Predicted;