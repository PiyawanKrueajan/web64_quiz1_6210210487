import AboutUs from '../components/AboutUs';
import Typography from '@mui/material/Typography';

function AboutUsPage () {

    return ( 
		<div >
			<div alige = "center">
				<Typography variant = "h4">
					คณะผู้จัดทำเว็บนี้
				</Typography>	
				
					<AboutUs name = "คนสวย "
						address = "ร้านเบเกอรี่"
						province = "เมืองคอน"/>
					<hr />
				
					<AboutUs name = "คนน่ารัก "
						address = "ร้านเสริมสวย"
						province = "เมืองสงขลา"/>
				
				
				
			</div>
				
		</div>



    );

}
export default AboutUsPage;