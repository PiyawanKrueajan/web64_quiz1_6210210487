import Predicted from '../components/Predicted';
import { useState } from "react";

import Grid from '@mui/material/Grid';
import { Typography, Box, Container } from '@mui/material/';
import Button from '@mui/material/Button';

function PredictedPage() {

    const [ name, setName ] = useState("");
    const [ horoscopeResult, setHoroscopeResult ] = useState(0);
    const [ translateResult, setTranslateResult ] = useState("");


    const [ brithday, setBrithday ] = useState("");
    const [ age, setAge ] = useState("");

    function calculateHoroscope(){
        let b = parseFloat(brithday);
        let a = parseFloat(age);
        let horoscope =  (b * a)/25 ;
        setHoroscopeResult( horoscope );
        if (horoscope > 20) {
            setTranslateResult("โชคดี มีแฟนสวย หล่อ ")
        }else{
            setTranslateResult("แฟนทิ้ง")
        }
    }

    return (
        <Container maxWidth='lg'>
            <Grid container spacing={2} sx={{ marginTop : "10px" }}>
                <Grid item xs={10}>
					
						<Typography variant="h4">
							ยินดีต้อนรับสู่เว็บทำนายดวง
						</Typography>
					
                </Grid>
					
				<Grid item xs={5}>
					<Box sx={{ textAlign : 'center'}} >
					
						คุณชื่อ: <input type="text"
                              value={name} 
                              onChange={ (e) => { setName(e.target.value); } } />  
							  <br />
							  
						วันเกิด: <input type="text" 
                              value={brithday} 
                              onChange={ (e) => { setBrithday(e.target.value); } } />  
							  <br />
							  
						อายุ: <input type="text"
                              value={age} 
                              onChange={ (e) => { setAge(e.target.value); } } />  
							  <br />

						<Button variant="contained" onClick={ ()=>{ calculateHoroscope() } }>
                            Calculate
						</Button>
					</Box>
				</Grid>
				
				<Grid item xs={3}>
                    
					{ horoscopeResult != 0 &&  
						<div>
							<hr />
						
							<Typography variant = "h5">
								ผลการทำนาย 
							</Typography>
							<Predicted 
								name={ name }
								horoscope = { horoscopeResult }
								result = { translateResult }
							 />
						</div>
					}
                </Grid>
            </Grid>            
        </Container>
    );
}

export default PredictedPage;
